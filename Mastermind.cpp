#include <memory>

#include "Mastermind.h"
#include "hlt/game.hpp"
#include "hlt/game_map.hpp"

using namespace hlt;

Mastermind::Mastermind(Game& game) : game(&game) {
    GameMap* map = this->game->game_map.get();
    this->height = map->height;
    this->width = map->width;
    this->spawn = this->game->me->shipyard->position;

    for (int i = 0; i < this->height; ++i) {
        this->game_map.push_back(std::vector<int>());
        for (int j = 0; j < this->width; ++j) {
            this->game_map[i].push_back(map->at(Position(i, j))->halite);
        }
    }
}

Mastermind::~Mastermind() {}

void Mastermind::init(Game& game) {
    Halite hal = 0;
    int travelValue = 0;

    for (int i = 0; i < height; ++i) {
        this->pathMap.push_back(std::vector<std::pair<std::list<Position>, double> >());
        for (int j = 0; j < width; ++j) {
            this->pathMap[i].push_back(asearch.aStarSearch(this->game_map, Pair(this->spawn.x, this->spawn.y), Pair(i, j)));

        }
    }

    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            missionBoard.push_back(Blackboard(i, j, this->pathMap[i][j].second));
        }
    }

    std::sort(missionBoard.begin(), missionBoard.end());
}

std::list<Position> Mastermind::setShipMission(hlt::Ship& s) {
    int i = 0;

    while (missionBoard[i].hasAssignee()) ++i;
    hlt::Position target = hlt::Position(missionBoard[i].getPosition().x, missionBoard[i].getPosition().y);
    missionBoard[i].setAssignee(&s);
    return this->pathMap[target.x][target.y].first;
}