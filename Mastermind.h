#pragma once

#include <vector>
#include <iostream>
#include <sstream>
#include <memory>
#include <algorithm>

#include "hlt/game.hpp"
#include "hlt/log.hpp"
#include "hlt/game_map.hpp"
#include "hlt/position.hpp"

#include "Blackboard.hpp"
#include "AStar.hpp"

class Mastermind
{
protected:
	hlt::Game* game;
	std::vector<std::vector<int> > game_map;
	std::vector<
		std::vector<
			std::pair<
				std::list<Position>,
				double
	> > > pathMap;
	int height;
	int width;
	std::vector<Blackboard> missionBoard;
	hlt::Position spawn;
	AStar asearch;

public:
	Mastermind(hlt::Game&);
	~Mastermind();

	void init(hlt::Game&);

	std::list<Position> setShipMission(hlt::Ship&);
};

