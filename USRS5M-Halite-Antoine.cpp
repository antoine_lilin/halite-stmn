// USRS5M-Halite-Antoine.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

/**
* Halite III Bot made by Antoine LILIN & Léo QUILLOT
*/

#include "hlt/game.hpp"
#include "hlt/constants.hpp"
#include "hlt/log.hpp"
#include "Mastermind.h"

#include <random>
#include <ctime>

using namespace std;
using namespace hlt;

Direction evaluateDirection(Position a, Position b) {
    Position c = Position(b.x - a.x, b.y - a.y);

    if (c.x == 1) return Direction::NORTH;
    else if (c.x == -1) return Direction::SOUTH;
    else if (c.y == 1) return Direction::EAST;
    else if (c.y == -1) return Direction::WEST;
    else return Direction::STILL;
}

int main(int argc, char* argv[]) {
    unsigned int rng_seed;
    if (argc > 1) {
        rng_seed = static_cast<unsigned int>(stoul(argv[1]));
    }
    else {
        rng_seed = static_cast<unsigned int>(time(nullptr));
    }
    mt19937 rng(rng_seed);

    Game game;
    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.

    // Creating main decision maker
    Mastermind master(game);
    std::unordered_map<std::shared_ptr<Ship>, std::list<Position> > shipPath;

    // Initializing decision maker with a* results for each map point
    master.init(game);

    game.ready("P6");

    for (;;) {
        game.update_frame();
        shared_ptr<Player> me = game.me;
        unique_ptr<GameMap>& game_map = game.game_map;

        vector<Command> command_queue;

        std::list<Position> positionList;
        for (const auto& ship_iterator : me->ships) {
            shared_ptr<Ship> ship = ship_iterator.second;
            if (shipPath[ship].empty()) {
                shipPath[ship] = master.setShipMission(*ship);
                log::log("Ship " + std::to_string(ship->id) + " moving towards " + shipPath[ship].front().to_string());
            }
            command_queue.push_back(ship->move(evaluateDirection(ship->position, shipPath[ship].front())));
            shipPath[ship].pop_front();
        }

        if (
            me->ships.size() < 10 &&
            game.turn_number <= 200 &&
            me->halite >= constants::SHIP_COST &&
            !game_map->at(me->shipyard)->is_occupied())
        {
            command_queue.push_back(me->shipyard->spawn());
        }

        if (!game.end_turn(command_queue)) {
            break;
        }
    }

    return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
