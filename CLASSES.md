

Ship :
- Attributes
    - current_destination { get }
    - current_path_to_destination { get }
    - position { get }
- Methods
    - decide
    - initPath

Blackboard :
- Attributes
    - board_map
- Methods
    - evaluate

StrategyEngine :
- Attributes
- Methods

StrategyPredictionEngine :
- Attributes
- Methods

MyMap :
- Attributes
    - 
- Methods