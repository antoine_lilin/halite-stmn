#pragma once

#include "hlt/entity.hpp"
#include "hlt/ship.hpp"
#include "hlt/position.hpp"

class Blackboard
{
protected:
	hlt::Position pos;
	double value = 0;
	hlt::Ship* assignee = nullptr;

public:
	Blackboard();
	Blackboard(int, int, double);
	Blackboard(hlt::Position, double);
	~Blackboard();

	bool hasAssignee() const;

	void setAssignee(hlt::Ship*);
	bool removeAssignee();

	void setValue(double);
	double getValue() const;

	void setPosition(hlt::Position&);
	void setPosition(int, int);

	hlt::Position const& getPosition() const;

	friend bool operator<(Blackboard const& a, Blackboard const& b) { 
		//hlt::log::log("Evaluating " + std::to_string(a.getValue()) + " to " + std::to_string(b.getValue()));
		return a.getValue() > b.getValue(); };
};

