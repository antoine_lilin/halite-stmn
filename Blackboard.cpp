#include "Blackboard.hpp"

Blackboard::Blackboard() {};

Blackboard::Blackboard(int x, int y, double val) {
	this->value = val;
	this->pos = hlt::Position(x, y);
}

Blackboard::Blackboard(hlt::Position pos, double val) : pos(pos), value(val) {}

Blackboard::~Blackboard() {};

bool Blackboard::hasAssignee() const {
	return !(this->assignee == nullptr);
}

void Blackboard::setAssignee(hlt::Ship* ship) {
	this->assignee = ship;
}

bool Blackboard::removeAssignee() {
	if (this->assignee == nullptr) return false;
	this->assignee = nullptr;
	return true;
}

void Blackboard::setValue(double val) {
	this->value = val;
}

double Blackboard::getValue() const {
	return this->value;
}

void Blackboard::setPosition(hlt::Position& pos) {
	this->pos = pos;
}

void Blackboard::setPosition(int x, int y) {
	this->pos = hlt::Position(x, y);
}

hlt::Position const& Blackboard::getPosition() const {
	return this->pos;
}