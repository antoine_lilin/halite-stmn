// A C++ Program to implement A* Search Algorithm
#include <vector>
#include <iostream>
#include <tuple>
#include <list>
#include <stack>
#include <queue>

#include "AStar.hpp"
#include "hlt/position.hpp"

using namespace hlt;

// Creating a shortcut for int, int pair type
typedef std::pair<int, int> Pair;
// Creating a shortcut for tuple<int, int, int> type
typedef std::tuple<double, int, int> Tuple;

AStar::AStar() {}

AStar::~AStar() {}

bool AStar::isValid(std::vector<std::vector<int> >& grid, const Pair& point)
{
	if (grid.size() > 0 && grid[0].size() > 0)
		return (point.first >= 0) && (point.first < (int)(grid.size()))
		&& (point.second >= 0)
		&& (point.second < (int)(grid[0].size()));

	return false;
}

bool AStar::isUnBlocked(std::vector<std::vector<int> >& grid, const Pair& point)
{
	return isValid(grid, point);
}

bool AStar::isDestination(const Pair& position, const Pair& dest)
{
	return position == dest;
}

double AStar::calculateHValue(const Pair& src, const Pair& dest)
{
	int64_t manhatan = std::max(abs(src.first - dest.first), abs(src.second - dest.second));

	return manhatan * 500;
}

double AStar::calculateTrajectValue(std::vector<std::vector<int> >& grid, std::list<Position> traject)
{
	double trajectSize = traject.size();
	double returnCost = 0;
	std::list<Position>::iterator it;
	for (it = traject.begin(); it != traject.end(); it++)
	{
		returnCost += 0.1 * grid[it->x][it->y];
	}

	return((-returnCost / trajectSize));
}

std::pair<std::list<Position>, double> AStar::aStarSearch(std::vector<std::vector<int> >& grid, const Pair& src, const Pair& dest)
{
	// If the source is out of range
	if (!isValid(grid, src)) {
		return std::pair<std::list<Position>, double>();
	}

	// If the destination is out of range
	if (!isValid(grid, dest)) {
		return std::pair<std::list<Position>, double>();
	}

	// Either the source or the destination is blocked
	if (!isUnBlocked(grid, src)
		|| !isUnBlocked(grid, dest)) {
		return std::pair<std::list<Position>, double>();
	}

	// If the destination cell is the same as source cell
	if (isDestination(src, dest)) {
		return std::pair<std::list<Position>, double>();
	}

	std::vector<std::vector<bool> > closedList;
	for (int i = 0; i < grid.size(); ++i) {
		closedList.push_back(std::vector<bool>());
		for (int j = 0; j < grid[0].size(); ++j) {
			closedList[i].push_back(false);
		}
	}

	std::priority_queue<Tuple, std::vector<Tuple>, std::greater<Tuple> > openList;
	std::array<std::array<cell, 64>, 64> cellDetails;

	int i;
	int j;

	// Initialising the parameters of the starting node
	i = src.first;
	j = src.second;

	cellDetails[i][j].f = 0.0;
	cellDetails[i][j].g = 0.0;
	cellDetails[i][j].h = 0.0;
	cellDetails[i][j].parent = { i, j };

	// Put the starting cell on the open list and set its
	// 'f' as 0
	openList.emplace(0.0, i, j);

	// We set this boolean value as false as initially
	// the destination is not reached.
	while (!openList.empty()) {
		const Tuple& p = openList.top();

		// Add this vertex to the closed list
		i = std::get<1>(p); // second element of tupla
		j = std::get<2>(p); // third element of tupla

		// Remove this vertex from the open list
		openList.pop();
		closedList[i][j] = true;

		for (int add_x = -1; add_x <= 1; add_x++) {
			for (int add_y = -1; add_y <= 1; add_y++) {
				//log::log("astar, evalue la case" + Position(add_x, add_y).to_string());
				if (add_x == 0 or add_y == 0)
				{
					Pair neighbour(i + add_x, j + add_y);
					if (isValid(grid, neighbour)) {
						if (isDestination(neighbour, dest)) {
							cellDetails[neighbour.first][neighbour.second].parent = { i, j };

							std::list<Position> path;
							std::stack<Pair> Path;

							int row = dest.first;
							int col = dest.second;

							Pair next_node = cellDetails[row][col].parent;
							do {
								path.push_front(Position(next_node.first, next_node.second));
								Path.push(next_node);
								next_node = cellDetails[row][col].parent;
								row = next_node.first;
								col = next_node.second;
							} while (cellDetails[row][col].parent != next_node);

							Path.emplace(row, col);
							int sum = 0;
							while (!Path.empty()) {
								Pair p = Path.top();
								Path.pop();
								sum += grid[p.first][p.second];
							}
							double trajectValue = calculateTrajectValue(grid, path);

							return std::pair<std::list<Position>, double>(path, trajectValue);
						}
						else if (!closedList[neighbour.first][neighbour.second]
						&& isUnBlocked(grid, neighbour)) {
							double gNew, hNew, fNew;
							gNew = cellDetails[i][j].g + 0.1 * grid[i][j];
							hNew = calculateHValue(neighbour, dest);
							fNew = gNew + hNew;

							if (cellDetails[neighbour.first][neighbour.second].f == -1
								|| cellDetails[neighbour.first][neighbour.second].f > fNew) {
								openList.emplace(fNew, neighbour.first, neighbour.second);

								cellDetails[neighbour.first][neighbour.second].g = gNew;
								cellDetails[neighbour.first][neighbour.second].h = hNew;
								cellDetails[neighbour.first][neighbour.second].f = fNew;
								cellDetails[neighbour.first][neighbour.second].parent = { i, j };
							}
						}
					}
				}

			}
		}
	}
}