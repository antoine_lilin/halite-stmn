# Strategies

## Global strat

Safety first (?)

Slow collect

Fast expand

Aggressive

Defend spawn

### Collection strategies

Early collection : 5 ships, need to collect quickly enough to get more

Late collection : Need to collect as much as possible to store and win the game

## Agent strat

Life first

Suicide

Avoid

Collect



# AI Structures

Behaviour tree

State Based

Common blackboard

# Board evaluation



# Opponent strategy prediction

