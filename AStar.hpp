#pragma once

#include <vector>
#include <tuple>

#include "hlt/position.hpp"

using namespace hlt;

typedef std::pair<int, int> Pair;
typedef std::tuple<double, int, int> Tuple;

// A structure to hold the necessary parameters
struct cell {
	// Row and Column index of its parent
	Pair parent;

	// f = g + h
	double f, g, h;
	cell() : parent(-1, -1), f(-1), g(-1), h(-1) {}
};

class AStar
{
public:
	AStar();
	~AStar();

	bool isValid(std::vector<std::vector<int> >&, const Pair&);
	bool isUnBlocked(std::vector<std::vector<int> >&, const Pair&);
	bool isDestination(const Pair&, const Pair&);
	double calculateHValue(const Pair&, const Pair&);
	double calculateTrajectValue(std::vector<std::vector<int> >& grid, std::list<Position>);
	std::pair<std::list<Position>, double> aStarSearch(std::vector<std::vector<int> >&, const Pair&, const Pair&);
};